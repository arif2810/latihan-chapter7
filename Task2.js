/**
 * Asynchronous
 * How to build a guessing game
 * Scenerio:
 * 1. User story: A user can enter a number
 * 2. User story: The system picks a random number from 1 to 6
 * 3. User story: If the user's number is equal to a random number, give the user 2 points
 * 4. User story: If the user's number is different than the random number by 1, give the user 1 point. Otherwise, give the user 0 points
 * 5. User story: The user can play the game as long as they want to
 */

let numbers = [1, 2, 3, 4, 5, 6];

let is_game_play = true;

let play = (time, work) => {
  return new Promise((resolve, reject) => {
    if(is_game_play){
      setTimeout(() => {
        resolve(work());
      }, time);
    }
    else{
      reject(console.log("The game can't play"));
    }
  });
};


play(0000, () => {
  let user_number = numbers[1];
  console.log(`"${user_number}" was selected`)
})
  .then(() => {
    return play(2000, () => {
      let random = numbers[1]
      console.log(`The system pick "${random}"`);
    });
  })

  // Belum nemu disini...

  .catch(() => {
    console.log("Game quit!");
  })

  .finally(() => {
    console.log("Play again?");
  })





